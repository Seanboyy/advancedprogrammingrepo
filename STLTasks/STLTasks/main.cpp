#include <random>
#include <vector>
#include <iostream>
#include <string>
#include <list>
#include <limits>
#include <queue>
#include <functional>

using std::numeric_limits;
using std::random_device;
using std::vector;
using std::normal_distribution;
using std::uniform_real_distribution;
using std::string;
using std::priority_queue;
using std::list;
using std::cout;
using std::endl;

///<summary>
///In class exercise
///</summary>
///<returns>Integer </returns>
int doItIntegerVector() {
	int count = 0;
	vector<int> theList;
	random_device generator;
	normal_distribution<double> distribution(20, 10);
	for (int a = 0; a < 10000; a++) {
		theList.push_back((int)distribution(generator));
	}
	for (int thing : theList) {
		if (thing == 1) {
			count++;
		}
	}
	return count;
}

string doItFloatList() {
	vector<float> theList;
	random_device generator;
	uniform_real_distribution<float> distribution(-numeric_limits<float>::min(), numeric_limits<float>::max());
	for (int a = 0; a < 10000; a++) {
		theList.push_back((float)distribution(generator));
	}
	for (float element : theList) {
		if (element == numeric_limits<float>::infinity()) return "found";
	}
	return "not found";
}

///<summary>
///
///</summary>
///<param name='mode'>1 for min, 2 for max</param>
int doItHeap(int mode) {
	switch (mode) {
	case 1:
		//TODO: do it
		break;
	case 2:
		//TODO: do it
		break;
	default:
		break;
	}
	return 0;
}

int main() {
	cout << doItIntegerVector() << endl;
	cout << doItFloatList() << endl;
	cout << doItHeap(1) << endl;
	cout << doItHeap(2) << endl;
	system("pause");
	return 0;
}