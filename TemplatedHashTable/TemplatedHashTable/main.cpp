/*
Sean Bamford
COMP 322
September 8, 2017
Main function used for testing header file
*/
#include "HashTable.h"
///<summary>
///Main method for testing
///</summary>
///<returns>0 if success, 1 if fail</returns>
int main() {
	try {
		//test creating a string keyed hash table
		HashTable<std::string, std::string, StringHash> table1(5, 0.75);
		//test inserting sample data
		table1.insert("a", "0");
		table1.insert("b", "1");
		table1.insert("c", "2");
		table1.insert("d", "3");
		table1.insert("e", "4");
		//print table
		std::cout << table1 << std::endl;
		//test search
		std::cout << "key a corresponds with " << table1.search("a") << std::endl;
		//test creating an int keyed hash table
		HashTable<int, int, IntHash> table2(5, 0.75);
		//test inserting data
		table2.insert(0, 0);
		table2.insert(1, 0);
		table2.insert(2, 2);
		table2.insert(3, 2);
		table2.insert(4, 3);
		//print table
		std::cout << std::endl << table2 << std::endl;
		//test search
		std::cout << "key 2 corresponds with " << table2.search(2) << std::endl;
		//test creating a float keyed hash table
		HashTable<int, float, FloatHash> table3(5, 0.75);
		//test inserting data
		table3.insert(8.0F, 0);
		table3.insert(0.34324F, 0);
		table3.insert(2.23F, 0);
		table3.insert(45.3F, 0);
		table3.insert(3.4F, 0);
		//print table
		std::cout << std::endl << table3 << std::endl;
		//test search
		std::cout << "key 45.3F corresponds with " << table3.search(45.3F) << std::endl;
		//test search for key that is not in table
		std::cout << "key f corresponds with " << table1.search("f") << std::endl;
		//wait for termination
		system("pause");
		//exit success
		return EXIT_SUCCESS;
	}
	catch (char* e) {
		std::cout << e << std::endl;
		system("pause");
		return EXIT_FAILURE;
	}
}