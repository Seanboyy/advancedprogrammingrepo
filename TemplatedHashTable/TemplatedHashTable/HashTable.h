/*
Sean Bamford
COMP 322
September 8, 2017
Header for creating templated and functored hash table data structure
*/
#ifndef HASH_TABLE_H_
#define HASH_TABLE_H_

#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <sstream>
#include <exception>

//
template<typename T, typename K>
struct Entry {
	Entry(K key, T value) {
		this->key = key;
		this->value = value;
	}
	K key;
	T value;
};

//templated class HashTable
//T: data type
//K: key type
//Functor: functor link
template<typename T, typename K, typename Functor>
class HashTable {
public:
	HashTable();
	HashTable(int);
	HashTable(int, double);
	void insert(K, T);
	T search(K);
	unsigned int hash(K);
	void checkIfNeedResize();
	void resize(int);
	void setSize(int);
	void setResizeRatio(double);
	int getSize();
	std::vector<std::list<Entry<T, K>>> getTable();
	//output operator for table
	friend std::ostream& operator<< (std::ostream& out, HashTable<T, K, Functor> table) {
		std::vector<std::list<Entry<T, K>>> table1 = table.getTable();
		for (int a = 0; a < table.getSize(); a++) {
			out << "Index " << a << " contains:\n";
			for (Entry<T, K> b : table1[a]) {
				out << "\t" << b.key << ": " << b.value << "\n";
			}
		}
		return out;
	}
	

private:
	//fields
	std::vector<std::list<Entry<T, K>>> table;
	int size;
	double currentRatio;
	double resizeRatio;
	std::list<Entry<T, K>> emptyList;
	Functor function;
};

//String key hashing
struct StringHash {
	unsigned int operator()(std::string key) {
		//create dummy hash object
		std::hash<std::string> a;
		//hash the key using STL string hash
		return a(key);
	}
};

//float key hashing
struct FloatHash {
	unsigned int operator()(float key) {
		//hash the key based on some random math operation
		return ((int)((key / 3.0) * 7.3));
	}
};

//int key hashing
struct IntHash {
	unsigned int operator()(int key) {
		//hash the key based on some random math operation
		return ((int)((key / 3.0) * 7.3));
	}
};


//default constructor for making empty table
template<typename T, typename K, typename Functor>
HashTable<T, K, Functor>::HashTable() {
	size = 0;
}
//constructor for size only
template<typename T, typename K, typename Functor>
HashTable<T, K, Functor>::HashTable(int size) {
	//add empty list containers to a vector
	for (int a = 0; a < size; a++) {
		table.push_back(emptyList);
	}
	this->size = size;
}
//constructor for both size and resizeRatio
template<typename T, typename K, typename Functor>
HashTable<T, K, Functor>::HashTable(int size, double resizeRatio) {
	//add empty list containers to a vector
	for (int a = 0; a < size; a++) {
		table.push_back(emptyList);
	}
	this->resizeRatio = resizeRatio;
	this->size = size;
}

//insert function
template<typename T, typename K, typename Functor>
void HashTable<T, K, Functor>::insert(K key, T value) {
	//create entry object
	Entry<T, K> entry(key, value);
	//call the hash function to sort data
	int index = hash(key) % size;
	//check if returned index is greater than the size
	if (index >= this->size) {
		while (index >= this->size) {
			if (index == 0) {
				resize(1);
			}
			else {
				resize(index);
			}

		}
	}
	//add the entry object to the list container in the vector
	table[index].push_back(entry);
	//check the resize ratio
	checkIfNeedResize();
}

//search funtion
//input key, return string containing data
template<typename T, typename K, typename Functor>
T HashTable<T, K, Functor>::search(K key) {
	//search though the table for the key
	for (Entry<T, K> b : table[hash(key) % size]) {
		if (b.key == key) {
			//return value of found key
			return b.value;
		}
	}
	throw "Key not found";
}

//hash function. calls functor specified in class declaration
template<typename T, typename K, typename Functor>
unsigned int HashTable<T, K, Functor>::hash(K key) {
	return function(key);
}

//check to see if the table is overfull vs the resize ratio
template<typename T, typename K, typename Functor>
void HashTable<T, K, Functor>::checkIfNeedResize() {
	int countused = 0;
	for (int a = 0; a < size; a++) {
		if (table[a].size() != 0) {
			countused++;
		}
	}
	currentRatio = (double)countused / (double)size;
	//add empty list containers until resize ratio is greater than current ratio
	while (currentRatio > resizeRatio) {
		table.push_back(emptyList);
		size++;
		std::vector<std::list<Entry<T, K>>> copy = table;
		for (int a = 0; a < size; a++) {
			table[a] = emptyList;
		}
		for (std::list<Entry<T, K>> a : copy) {
			for (Entry<T, K> b : a) {
				insert(b.key, b.value);
			}
		}
		currentRatio = (double)countused / (double)size;
	}
}

//resize function for adding elements to the end
template<typename T, typename K, typename Functor>
void HashTable<T, K, Functor>::resize(int size) {
	int counts = size - this->size;
	while (counts >= 0) {
		table.push_back(emptyList);
		counts--;
		this->size++;
		std::vector<std::list<Entry<T, K>>> copy = table;
		for (int a = 0; a < size; a++) {
			table[a] = emptyList;
		}
		for (std::list<Entry<T, K>> a : copy) {
			for (Entry<T, K> b : a) {
				insert(b.key, b.value);
			}
		}
	}
}

//setter for size field
template<typename T, typename K, typename Functor>
void HashTable<T, K, Functor>::setSize(int size) {
	this->size = size;
}

//setter for resize ratio field
template<typename T, typename K, typename Functor>
void HashTable<T, K, Functor>::setResizeRatio(double resizeRatio) {
	this->resizeRatio = resizeRatio;
}

//getter for size field
template<typename T, typename K, typename Functor>
int HashTable<T, K, Functor>::getSize() {
	return this->size;
}

//getter for table structure
template<typename T, typename K, typename Functor>
std::vector<std::list<Entry<T, K>>> HashTable<T, K, Functor>::getTable() {
	return this->table;
}

#endif
